package com.example.myapplication;

import android.os.Environment;

public class Constants {

    public final static int HOME_RESULT = 100;
    public final static String FILE_NAME = "file_name";
    public final static String FILE_PATH = "file_path";
    public final static int VIDEO = 1;
    public static final int PERMISSIONS_WRITE_EXTERNAL_STORAGE = 1;
    public static final String FILE_LOCATION = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES)
            + "/Test";
}
