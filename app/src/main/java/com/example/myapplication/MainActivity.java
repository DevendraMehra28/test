package com.example.myapplication;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.myapplication.trimmer.VideoTrimListener;
import com.example.myapplication.trimmer.VideoTrimmerView;


public class MainActivity extends AppCompatActivity implements VideoTrimListener {


    private Context context;
    private String path;
    private ProgressDialog mProgressDialog;
    private VideoTrimmerView trimmerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = this;
        setContentView(R.layout.activity_main);
        trimmerView = findViewById(R.id.trimmer_view);

        path = getIntent().getStringExtra(Constants.FILE_PATH);

        Log.d("Log12", "" + path);

        if (trimmerView != null) {
            trimmerView.setOnTrimVideoListener(this);
            trimmerView.initVideoByURI(Uri.parse(path));

            Log.d("Log12", "insider");

        }

    }


    @Override
    public void onStartTrim() {
        buildDialog(getResources().getString(R.string.trimming)).show();
    }

    @Override
    public void onFinishTrim(String url) {
        if (mProgressDialog.isShowing()) mProgressDialog.dismiss();
        Toast.makeText(this, getString(R.string.trimmed_done), Toast.LENGTH_SHORT).show();
        finish();
    }

    @Override
    public void onCancel() {
        trimmerView.onDestroy();
        finish();
    }


    private ProgressDialog buildDialog(String msg) {
        if (mProgressDialog == null) {
            mProgressDialog = ProgressDialog.show(this, "", msg);
        }
        mProgressDialog.setMessage(msg);
        return mProgressDialog;
    }
}