package com.example.myapplication.trimmer;

/**
 * Created by sandeepsaini on 03,July,2019
 */
public interface SingleCallBack<T, V> {
    void onSingleCallback(T t, V v);
}

