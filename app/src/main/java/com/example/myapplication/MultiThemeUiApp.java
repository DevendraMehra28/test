package com.example.myapplication;

import android.app.Application;
import android.content.Context;
import android.util.Log;

import com.example.myapplication.utils.BaseUtils;

import nl.bravobit.ffmpeg.FFmpeg;

/**
 * Created by sandeepsaini on 04,July,2019
 */
public class MultiThemeUiApp extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        BaseUtils.init(this);
        initFFmpegBinary(this);
    }

    private void initFFmpegBinary(Context context) {
        if (!FFmpeg.getInstance(context).isSupported()) {
            Log.e("Log16","Android cup arch not supported!");
        }
    }
}
